/**
 * Created by thangchung on 1/6/14.
 */
define(['$', 'mazweb/test'], function($, test) {
    $(function() {
        test.init();
    });
});