## The Magazine Website

The website that implemented by using [Lavarel framework](http://laravel.com/docs). We used [Grunt](http://gruntjs.com) - the Javascript Task Runner - to make all thing like minify, uglify and compile the resourses (js, css). And we also using less in this website.
To make the front-end code more manageable, we used [requiresjs](http://requirejs.org) for optimize js loading when the website start.

## Official Documentation

Documentation for the entire framework can be found on the [wiki](https://bitbucket.org/thangchung/mazweb-laravel/wiki/Home).

### Contributing To This Project

**All issues and pull requests should be filed on the [mazweb](https://bitbucket.org/thangchung/mazweb-laravel) repository.**